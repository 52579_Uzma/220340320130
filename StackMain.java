class Stack
{
	
	int top1;
		int top2;
	int size ;
	int[] a  ;
	Stack(int n)
	{
		size = n ;
		a = new int[n] ;
		top1 = n/2+1 ; 
		top2 = n/2;
	}
	
	public void push1(int val)
	{
		if(top1 >0)
		{
			top1--;
			a[top1] = val;
		}
		else
		{
			System.out.println("Stack Overflow "+"By element : "+val);
			System.exit(1);
		}
	}
	public void push2(int val)
	{
		if(top2 <size-1)
		{
			top2++ ;
			a[top2] = val;
		}
		else
		{
			System.out.println("Stack Overflow "+" by element : "+val);
			
		}
	}
	public int pop1()
	{
		if(top1 <=size/2)
		{
			int val=a[top1] ;
			top1++ ;
			return val ;
		}
		else
		{
			System.out.println("Stack underflow ");
			System.exit(1);
		}
		return 0 ;
	}
	public int pop2()
	{
		if(top2 >=size/2+1)
		{
			int val=a[top2] ;
			top2-- ;
			return val ;
		}
		else
		{
			System.out.println("Stack underflow ");
			System.exit(1);
		}
		return 1 ;
	}
	
}
class StackMain
{
	public static void main(String args[])
	{
		
		Stack s = new Stack(10);
		s.push1(5);
		s.push2(10);
		s.push2(15);
		s.push1(11);
		s.push2(7);
		s.push2(40);
		System.out.println("Popped element from stack1 is "+s.pop1());
		System.out.println("Popped element from stack2 is "+s.pop2());
	}		
}