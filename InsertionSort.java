import java.util.*;

class InsertionSort
{
	public static void sort(int[] a1 , int n)
	{
		
		for(int i=0 ; i<n ; i++)
		{
			
			int j ;
			int temp = a1[i] ;
			for(j=i-1 ; j>=0 && a1[j]>temp ; j-- )
			{
				
				a1[j+1] = a1[j] ;
				printArray(a1 ,n);
			}
			
			a1[j+1] = temp ;
			
		}
					
	}
	static void printArray(int[] a1 , int n)
	{
		for(int i=0 ;i<n ; i++)
		{
			
	
			System.out.print(a1[i]+" ");

		}
		System.out.println();
	}
	
	public static void main(String args[])
	{
		Scanner sc = new Scanner(System.in);
		
		//String s = sc.nextLine();
		int n = sc.nextInt();
		//String[] str = s.split(" ");
		//		int m = str.length ;
		int[] a1 = new int[n];
		for(int i=0 ; i<n ; i++)
		{
			//a1[i] = Integer.parseInt(str[i]);
			a1[i] = sc.nextInt();
		}
		InsertionSort.sort(a1 , n);
	}
}

