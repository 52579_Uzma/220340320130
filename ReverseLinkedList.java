import java.util.*;
class Test 
{
	Scanner sc = new Scanner(System.in);
	Node head ;
	class Node
	{
		int data ;
		Node next ;
		Node(int val)
		{
			data = val ;
			next = null ;
		}
	}
	public void add(int data)
	{
		Node newNode = new Node(data);
		if(head == null)
		{
			head = newNode ;
		}
		else
		{
			Node trav = head ;
			while(trav.next != null)
			{
				trav = trav.next ;
				
			}
			trav.next = newNode;
		}
	}
	public Node reverse()
	{
		Node trav = head ;
		Node prev = null ;
		while(trav != null)
		{
			Node temp = trav.next  ;
			trav.next = prev ;
			prev = trav ;
			trav = temp  ;
		}
		return prev ;
	}
	public void display()
	{
		Node trav = head ;
		while(trav != null)
		{
			System.out.print(trav.data+" ");
			trav = trav.next ;
		}
		
	}
	
}
class ReverseLinkedList
{
	
	
	public static void main(String args[])
	{
		Scanner scan = new Scanner(System.in);
		Test t = new Test();
		int noOfElements = scan.nextInt();
		int val=0;
		for(int i=0 ; i<noOfElements ; i++)
		{
			val = scan.nextInt();
			
			t.add(val);
		}
		t.head =t.reverse();
		t.display();
	}
}